from app.businesslogic.perfect_numbers import NumberClassifier
from app.common.exceptions import PerfectNumberException
from app.common.validation_message import ValidationMessage
from app.entities.enumeration.result import Result
import unittest


class FakeLogger:

    @staticmethod
    def log(logitem):
        print('request is logged: {0}'.format(str(logitem)))


class TestNumberClassifierYoYoCrawler(unittest.TestCase):

    def setUp(self):
        self._sut = NumberClassifier(logger=FakeLogger())

    def assertRaisesWithMessage(self, msg, func, exception_type, *args, **kwargs):
        try:
            func(*args, **kwargs)
            self.assertFail()
        except exception_type as inst:
            self.assertEqual(inst.message, msg)

    def test_fail_if_input_number_was_invalid(self):
        number = 'ABC'
        message = ValidationMessage.InvalidNumber.format(number)
        self.assertRaisesWithMessage(message, lambda: self._sut.classify(number), PerfectNumberException)

    def test_fail_if_input_number_was_invalid(self):
            number = 0
            message = ValidationMessage.ZeroInput
            self.assertRaisesWithMessage(message, lambda: self._sut.classify(number), PerfectNumberException)

    def test_should_classify_perfect(self):

        self.assertEqual(self._sut.classify(6).result, Result.perfect)
        self.assertEqual(self._sut.classify(28).result, Result.perfect)
        self.assertEqual(self._sut.classify(496).result, Result.perfect)
        self.assertEqual(self._sut.classify(8128).result, Result.perfect)
        self.assertEqual(self._sut.classify(33550336).result, Result.perfect)

    def test_should_classify_abundant(self):

        self.assertEqual(self._sut.classify(12).result, Result.abundant)
        self.assertEqual(self._sut.classify(18).result, Result.abundant)
        self.assertEqual(self._sut.classify(200).result, Result.abundant)

    def test_should_classify_deficient(self):

        self.assertEqual(self._sut.classify(5).result, Result.deficient)
        self.assertEqual(self._sut.classify(8).result, Result.deficient)
        self.assertEqual(self._sut.classify(199).result, Result.deficient)