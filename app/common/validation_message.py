class ValidationMessage:
    GeneralServerFail = 'Server failed to operate. please contact the administrators.'
    InvalidNumber = 'Entered an invalid number. {0}'
    ZeroInput = 'Number must be greater than 0'
