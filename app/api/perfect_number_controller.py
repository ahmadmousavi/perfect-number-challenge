from app.businesslogic.perfect_numbers import NumberClassifier
from app.common.exceptions import PerfectNumberException
from app.common.validation_message import ValidationMessage
from flask_restful import Resource
from flask import request
from flask import jsonify


class PerfectNumberController(Resource):
    def __init__(self, logger=None):
        self.logger = logger

    '''
    accepts a number and classifies the number
    into three categories: perfect, abundant and deficient
    '''

    def get(self):
        try:

            # querystring args

            number = request.args.get('number')
            if number is None:
                number = eval(request.args.keys().__next__()).get('number', '')

            response = NumberClassifier(logger=self.logger).classify(number)
            return jsonify({'success': True, 'class': response.result})
        except PerfectNumberException as ex:
            return jsonify({'success': False, 'errors': [ex.message]})
        except Exception:
            return jsonify({'success': False, 'errors': [ValidationMessage.GeneralServerFail]})
