'use strict';

   var app = angular
		.module('digitalfineprintApp', ['ui.router', 'ngResource', 'ngMessages'])
		.config(['$stateProvider', '$urlRouterProvider', function config($stateProvider, $urlRouterProvider) {
		    $urlRouterProvider.otherwise('/perfectnumber');
		    $stateProvider
				.state('perfectnumber', {
				    url: '/perfectnumber',
				    templateUrl: '/static/doc/app/perfectnumber/perfectnumber.html',
				    controller: 'PerfectNumberCtrl',
				    displayName: 'perfectnumber api'
				});
   }]);