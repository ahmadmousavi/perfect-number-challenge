'use strict';

angular
    .module('digitalfineprintApp')
	.controller('PerfectNumberCtrl', ['$rootScope', '$scope',
        function PerfectNumberCtrl($rootScope, $scope) {

        	var util = new RestUtil(
        	    {
        	        'title': 'Record new history',
        	        'headers' : [],
        	        'data': [{label:'number', example:40 }],
        	        'route': 'api/perfectnumber',
        	        'baseAddress': serverAddress,
        	        'element': '.perfectNumber',
        	        'httpverb': 'GET'
        	    });
        	    
        	    util.render();
}]);