//Author: Ahmad Mousavi
//email: sam683@gmail.com
//this class will generate test ui for your rest methods
function RestUtil(op) {

    var options = {
        title: '',
        example: '',
        headers: [],
        data: [],
        element: '',
        httpVerb: '',
        route: '',
        baseAddress: '',
        _id : ''
    };
    
    var self = this;

    $.extend(options, op);
    

    this.getOptions = function() {
        return options;
    }
    
    var generateUUID  = function() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    };

    //Draw the ui
    this.render = function() {
        
    options._id = generateUUID();    
        
    var markup = 
'<div class=""><div class="toggle" style="width: 82px; border-radius: 3px; background-color: whitesmoke; padding-left: 8px; cursor: pointer;">collapse</div>\
  <div class="well well-large">\
    <fieldset>\
    <legend><span class="label label-default" style="margin-right:5px;">##verb</span>##title</legend>\
<label>Endpoint</label>\
<input type="text" placeholder="Type something…" class="input-large" value="##endpoint" id=endpoint_'+options._id+' style="width: 100%;height: 20px;">\
<span class="label label-info">header: ##hCount </span>\
<table class="table table-striped">\
<thead>\
</thead>\
  <tbody>\
   ##header\
  </tbody>\
</table>\
<span class="label label-info">data: ##dCount </span>\
  <table class="table table-striped">\
<thead>\
</thead>\
  <tbody>\
   ##data\
  </tbody>\
</table>\
<button type="submit" class="btn" id='+options._id+'>Submit</button>\
  </fieldset>\
</div>\
<div id=result_'+options._id+'></div>\
</div>';
    
    
    var headers = '';
    $.each(options.headers, function(i,v){
        headers+= '<tr><td>'+ v +'</td><td><input type="text"  style="width:100%" class="input-large" value="" id="header_'+ v +'_'+options._id+'"></td></tr>';
    });
    
    var data = '';
    $.each(options.data, function(i,v){
        label = (v['label']? v['label']: v);
        data+= '<tr><td>'+ label +'</td><td><input type="text" style="width:100%"  class="input-large" value="" id="body_'+ label +'_'+options._id+'"></td></tr>';
        if(v['example'])
            data+= '<tr style="font-family: monospace;color: #0077b3;"><td>'+label+' example: </td><td> '+ v['example'] +'</td></tr>';
    });


    //set title
    markup= markup.replace('##title', options.title);
    markup= markup.replace('##verb', options.httpverb);
    markup= markup.replace('##endpoint', options.baseAddress + '/' + options.route);
    markup= markup.replace('##header', headers);
    markup= markup.replace('##data', data);
    markup= markup.replace('##hCount', options.headers.length);
    markup= markup.replace('##dCount', options.data.length);


    $(options.element).html(markup);


    //wire some event
    $('#'+options._id).on('click', function(){
        self.call();
    });

    }

    this.call = function() {

        var _data = {};
        var endpoint= $('#endpoint_'+options._id).val();
        var resultElem = $('#result_'+options._id);

        $.each(options.data, function(index, value) {
             value = (value['label']? value['label']: value);
            _data[value] =$('#body_' + value + '_' + options._id).val();
        });

        $.ajax({
            method: "",
            type: options.httpverb,
            beforeSend: function(request) {
                $.each(options.headers, function(index, value) {
                    request.setRequestHeader(value, $('#header_' + value + '_' + options._id).val());
                });
            },
            url: endpoint,
            data: JSON.stringify(_data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            processData: false,
            success: function(msg) {
                $(resultElem).html('<div class="alert alert-info"><strong>result : </strong><div style="overflow-x:auto;">'+ JSON.stringify(msg) +'</div></div>')
            },
            error: function(msg){
                $(resultElem).html('<div class="alert alert-error"><strong>result : </strong><span>'+ msg.statusText +'</span></div>')
            }
        });
    }

}
