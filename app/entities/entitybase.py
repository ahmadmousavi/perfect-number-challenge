import time


class EntityBase(object):
    def __init__(self, data=None):
        self.created_by = ''
        self.created_at = time.time()
        self.modified_by = ''
        self.modified_at = ''
        self.update_from_dic(data)

    # Update instance with a dictionary
    def update_from_dic(self, dic):
        if dic:
            for attr in dic:
                if hasattr(self, attr):
                    setattr(self, attr, dic[attr])
