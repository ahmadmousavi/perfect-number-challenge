from flask import Flask
from flask_restful import Api
from flask import render_template
from app.common.constants import SERVER_IP, SERVER_PORT
from app.api.perfect_number_controller import PerfectNumberController

from app.common.logger import Logger

app = Flask(__name__)

api = Api(app)
logger = Logger()
api.add_resource(PerfectNumberController, '/api/perfectnumber'
                 , resource_class_kwargs={'logger': logger})


@app.route('/doc')
def index():
    return render_template('doc/doc.html', serverAddress='http://{0}:{1}'
                           .format(SERVER_IP, SERVER_PORT)), 200


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.errorhandler(401)
def not_found(error):
    return render_template('401.html'), 401