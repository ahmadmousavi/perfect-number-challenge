from app.entities.enumeration.result import Result
from app.entities.response_object import ResponseObject
from app.common.exceptions import PerfectNumberException
from app.common.validation_message import ValidationMessage
import math


class NumberClassifier:
    def __init__(self, logger=None):
        self.logger = logger

    def classify(self, number):
        self.logger.log('started processing for the input {0}'.format(number))

        if not str(number).isnumeric():
            raise PerfectNumberException(ValidationMessage.InvalidNumber.format(number))

        number = int(number)

        if number <= 0:
            raise PerfectNumberException(ValidationMessage.ZeroInput)

        sequence = []
        for num in range(2, math.ceil(math.sqrt(number))):
            division = number / num
            if division == int(division):
                sequence.append(int(division))
                sequence.append(num)

        if number > 1:
            sequence.append(1)

        summation = sum(sequence)

        if summation == number:
            result = Result.perfect
        elif summation > number:
            result = Result.abundant
        else:
            result = Result.deficient

        return ResponseObject(result)
