## Perfect Number Challenge
A flask rest api with python 3.6


### Description:
This project will classifiy a given number into three categories: 
perfect, abundant and deficient.


### API specification

Address: defaul ip and port are kept in app/common/connstants.py
```
SERVER_IP = '127.0.0.1'
SERVER_PORT = 65432

[GET] http://127.0.0.1:65432/api/perfectnumber

```

Input: input data is a querystring which holds one parametrers: number
```

example: http://127.0.0.1:65432/api/perfectnumber?number=6500

```

Output: it is a json datastructure as below:
```
{"class":"abundant","success":true}
```

### Angular JS UI to test the api
This project also provides a UI built in Angularjs 1.3 to test the API.
```
http://127.0.0.1:65432/doc
```


### install
```
	python version 3.6
	
    virtualenv env/
    source env/bin/activate
    pip install -r requirements.txt
    python wsgi.py

```

### test
```
python perfect_numbers_tests.py
```

